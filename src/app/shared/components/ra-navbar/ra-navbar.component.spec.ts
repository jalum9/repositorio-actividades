import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaNavbarComponent } from './ra-navbar.component';

describe('RaNavbarComponent', () => {
  let component: RaNavbarComponent;
  let fixture: ComponentFixture<RaNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
