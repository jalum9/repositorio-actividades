import {BrowserModule}     from '@angular/platform-browser';
import {NgModule}          from '@angular/core';
import {RaNavbarComponent} from './ra-navbar/ra-navbar.component';

@NgModule({
  imports: [
    BrowserModule,
  ],
  exports: [
    RaNavbarComponent,
  ],
  declarations: [
    RaNavbarComponent,
  ]
})
export class ComponentsModule {
}
