import {NgModule}      from '@angular/core';
import {CommonModule}  from '@angular/common';
import {MainComponent} from './main.component';
import {MainRouting}   from './main.routing';

@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRouting
  ]
})
export class MainModule {
}
