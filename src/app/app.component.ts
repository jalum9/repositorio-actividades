import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <ra-navbar></ra-navbar>
    <div class="ui container">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {}
